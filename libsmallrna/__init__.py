__copyright__ = "Copyright (C) 2020-2021 Blaise Li"
__licence__ = "GNU GPLv3"
from .libsmallrna import (
    has_pi_signature, has_endosi_signature, has_26G_signature, has_csr1_endosi_signature,
    count_small,
    count_annots, count_nucl, count_first_bases,
    add_compositions, add_results, Composition, get_read_info,
    PI_MIN, PI_MAX, SI_MIN, SI_MAX,
    SI_SUFFIXES, SI_PREFIXES,
    SMALL_TYPES_TREE,
    type2RNA,
    types_under,
    SMALL_TYPES,
    SI_TYPES,
    SIU_TYPES,
    SISIU_TYPES,
    RMSK_SISIU_TYPES,
    JOINED_SMALL_TYPES)
