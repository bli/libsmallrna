# Utilities to identify small RNAs in C. elegans high throughput sequencing data


## Installing

Get the source using `git clone git@gitlab.pasteur.fr:bli/libsmallrna.git`,
`cd` into it and run `python3 -m pip install .`

It might also work directly:

    python3 -m pip install git+ssh://git@gitlab.pasteur.fr/bli/libsmallrna.git


## Citing

If you use this package, please cite the following paper:

> Barucci et al, 2020 (doi: [10.1038/s41556-020-0462-7](https://doi.org/10.1038/s41556-020-0462-7))
